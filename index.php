<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Visitkortshanterare</title>
		<link rel="icon" type="image/png" href="img/favicon-32x32.png" sizes="32x32" />
		<link rel="icon" type="image/png" href="img/favicon-16x16.png" sizes="16x16" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<link href="https://fonts.googleapis.com/css?family=Overlock|Tangerine" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/index.css">
		<script src="js/index.js"></script>
	</head>
	<body>

	<!-- Webbsidans sidhuvud -->
	<div class="jumbotron text-center">
		<h1>Visitkortshanteraren</h1>
		<p>&#8765; Lägg till, redigera och titta på dina egna visitkort &#8764;</p>
	</div>

	<div class="container-fluid">
		<p>Visitkortshanteraren är tjänsten som tillhandhåller dig möjligheten att skapa, redigera och beundra visitkort som du själv har skapat. Använd flikarna nedan för att navigera dig framåt.</p>
	</div>

	<?php
		// Kollar om det finns några parametrar från formuläret som kan stoppas in i databasen.
		include 'php/uploadCard.php';

		// Kollar om en fil ska raderas.
		if (isset($_POST["delete"])) 
			// Raderar visitkortet.
			include 'php/deleteCard.php';
	?>

	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#tab1">Skapa ett nytt visitkort</a></li>
		<li><a data-toggle="tab" href="#tab2">Visa alla befintliga visitkort</a></li>
	</ul>

	<div class="tab-content">
		<div id="tab1" class="tab-pane fade in active">
			<br>
			<ul class='list-group'>
				<li class='list-group-item'>
					<!-- Uppladdningsformulär för att skapa visitkort -->
					<?php include 'php/getForm.php';?>
				</li>
			</ul>
		</div>
		<div id="tab2" class="tab-pane fade">
			<br>
			<!--<div class="input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
				<input id="search" class="form-control" name="search" placeholder="Sök efter visitkort...">
			</div><br> -->
			<!-- Lista över alla befintliga visitkort -->
			<?php include 'php/fetchCards.php';?>
		</div>
	</div>
	</body>
</html>