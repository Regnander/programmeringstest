<?php
	// Kollar om alla inmatningsfält har fyllts i.
	if(isset($_POST["name"]) && isset($_POST["surName"]) && isset($_POST["telephone"]) && isset($_POST["email"]))
	{
		try {
			// Försöker ansluta till databasen.
			$connection = pg_connect("host=localhost port=5432 dbname=vcard user=postgres password=losen");

			// Kollar om anslutningen fungerar. "fetchCards.php" meddelar användaren om anslutningen skulle misslyckas.
			if ($connection) {
				// Skickar en begäran till databasen om att stoppa in användarens inmatade värden.
				$result = pg_query($connection, "INSERT INTO table_name VALUES ('" . $_POST["name"] . "', '" . $_POST["surName"] . "', '"
					. $_POST["telephone"] . "', '" . $_POST["email"] . "', " ./* base64_encode($_POST["image"]) .*/ "null);");
				// Meddelar användaren att allting gick bra.
				echo "<div class='alert alert-success'>Visitkortet för <i>" . $_POST["name"] . " " . $_POST["surName"] . "</i> har sparats och kan nu ses under den tredje fliken.</div>";
			}
		} catch (Exception $e){
			// Meddelar användaren att allting inte gick speciellt bra.
			echo "<div class='alert alert-danger'>Ett fel uppstod under databaskommunikationen: " . $e->getMessage() . "</div>";
		}
	}
?>