<?php
	try {
		// Försöker ansluta till databasen.
		$connection = pg_connect("host=localhost port=5432 dbname=vcard user=postgres password=losen");

		// Kollar om anslutningen misslyckades.
		if (!$connection) {
			echo "<div class='alert alert-danger'>Denna tjänst kan för tillfället inte anslutas till databasen.</div>";
		} else {
			// Begär att hämta alla rader från databastabellen.
			$reply = pg_query($connection, "SELECT * FROM table_name;");
			// Kollar hur många rader tabellen har.
			$amountOfRows = pg_num_rows($reply);

			// Kollar om fler än en rad hämtades.
			if ($amountOfRows > 0) {
				// Öppnar listan.
				echo "<ul class='list-group'>";

				// Går igenom alla tabellrader.
				for($a = 0; $a < $amountOfRows; $a++) {
					// Hämtar all data från aktuell rad i tabellen.
					$row = pg_fetch_row($reply);

					// Sammanställer ett visitkort med data från databasen, bestående av en bild till vänster, namn och kontaktinfo i mitten samt knappar till höger.
					echo
						"<li class='list-group-item'>
							<div class='media'>
								<div class='media-left'>
									<img src='data:image/png;base64," . base64_decode($row[5]) . "' class='media-object' style='width:60px'>
								</div>
								<div class='media-body'>
									<h4 class='media-heading'>" . $row[1] . " " . $row[2] . "</h4>
									<span class='glyphicon glyphicon-phone-alt'></span> <a href='tel:" . $row[3] . "'>" . $row[3] . "</a><br>
									<span class='glyphicon glyphicon-envelope'></span> <a href='mailto:" . $row[4]. "'>" . $row[4] . "</a>
								</div>
								<div class='media-right'>
									<div class='btn-group-vertical btn-group-sm' role='group' aria-label='Basic example'>
										<form method='POST'>
											<button type='submit' class='btn btn-secondary' value='". $row[0] . "' name='updateForm' data-toggle='tooltip' data-placement='left' title='Redigera'>
												<span class='glyphicon glyphicon-pencil'></span>
											</button>
										</form>
										<form method='POST'>
											<button type='submit' class='btn btn-secondary' value='". $row[0] . "' name='delete' data-toggle='tooltip' data-placement='left' title='Radera'>
												<span class='glyphicon glyphicon-trash'></span>
											</button>
										</form>
									</div>
								</div>
							</div>
						</li>";
					}

				// Stänger listan.
				echo "</ul>";
			} else {
				// Meddelar att det inte finns några visitkort att visa.
				echo "<div class='alert alert-info' role='alert'>Det finns inga visitkort att visa.</div>";
			}
		}
	} catch(Exception $e) {
	}
?>