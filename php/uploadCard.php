<?php
	// Kollar om alla inmatningsfält har fyllts i (variabeln "bild" verkar inte fungera med isset???).
	if (isset($_POST["name"]) && isset($_POST["surName"]) && isset($_POST["telephone"]) && isset($_POST["email"])) {
		try {
			// Försöker ansluta till databasen.
			$connection = pg_connect("host=localhost port=5432 dbname=vcard user=postgres password=losen");

			//print_r($_FILES);
			//echo "<img src='data:image/png;base64," . base64_encode(base64_decode(base64_encode(file_get_contents($_FILES['image']['tmp_name'])))) . "' class='media-object' style='width:60px'>";

			// Kollar om anslutningen fungerar. "fetchCards.php" meddelar användaren om anslutningen skulle misslyckas, varav detta undviks här.
			if ($connection) {
				// Kollar om databasinstansen ska uppdateras genom att söka efter angivet id och se om antalet rader är större än noll (då en rad har alltså hittats).
				if (pg_num_rows(pg_query($connection, "SELECT * FROM table_name WHERE \"id\" = '" . $_POST["id"] . "';")) > 0) {
					$query = "UPDATE table_name SET \"name\" = '" . $_POST["name"] . "', \"surName\" = '" . $_POST["surName"] . "', \"telephone\" = '"
						. $_POST["telephone"] . "', \"email\" = '" . $_POST["email"] . "', \"image\" = '"
						. /*pg_escape_bytea*/(base64_encode(file_get_contents($_FILES['image']['tmp_name']))) . "' WHERE \"id\" = '" . $_POST["id"] . "';";
					$successMessage = "Visitkortet för <i>" . $_POST["name"] . " " . $_POST["surName"] . "</i> har uppdaterats och kan nu ses i sin uppdaterade form i den tredje fliken.";
				} else {
					$query = "INSERT INTO table_name VALUES ('" . $_POST["id"] . "', '" . $_POST["name"] . "', '" . $_POST["surName"] . "', '"
						. $_POST["telephone"] . "', '" . $_POST["email"] . "', '" . /*pg_escape_bytea*/(base64_encode(file_get_contents($_FILES['image']['tmp_name']))) . "');";
					$successMessage = "Visitkortet för <i>" . $_POST["name"] . " " . $_POST["surName"] . "</i> har sparats och kan nu ses under i tredje fliken.";
				}
				
				// Skickar en begäran till databasen om att stoppa in användarens inmatade värden.
				$reply = pg_query($connection, $query);

				// Kollar om det gick bra.
				if ($reply != false)
					// Meddelar användaren att det gick bra.
					echo "<div class='alert alert-success'>" . $successMessage . "</div>";
				else
					// Meddelar användaren att det inte gick bra.
					echo "<div class='alert alert-danger'>Ett fel uppstod under databaskommunikationen.</div>";
			}
		} catch (Exception $e) {
			echo "<div class='alert alert-danger'>Fel: " . $e->getMessage() . "</div>";
		}
	}
?>