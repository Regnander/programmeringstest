<?php
	try {
		// Kollar om parametern används.
		if (isset($_POST['updateForm'])) {
			// Ansluter till databasen.
			$connection = pg_connect("host=localhost port=5432 dbname=vcard user=postgres password=losen");

			// Kollar om ansluten lyckades.
			if ($connection) {
				// Begär att hämta databastabellraden som har angivet ID.
				$reply = pg_query($connection, "SELECT * FROM table_name WHERE \"id\" = '" . $_POST['updateForm'] . "';");
				// Lagrar den aktuella raden.
				$row = pg_fetch_row($reply);

				//echo "SELECT * FROM table_name WHERE \"id\" = '" . $_POST['updateForm'] . "' '". $row[0] ."' '".$row[1] ."' '".$row[2] ."' '".$row[3] ."' '".$row[4] ."' '".$row[5];

				//echo "<br>". pg_num_rows($reply). " rader hittades";

				// Lägger till ett inmatningfält med visitkortets ID som inte går att ändras.
				$id = "<input type='text' id='inputID' name='id' value='" . $row[0] . "' style='visibility:hidden; height:0px' readonly>";
				// Resten fylls i så användaren kan ändra bäst den vill.
				$name = $row[1];
				$surName = $row[2];
				$telephone = $row[3];
				$email = $row[4];
				$image = $row[5];
			}
		} else {
			// Lägger till ett tomt inaktiverat inmatningsfält för ID, då det ska skötas programmatiskt.
			$id = "<input type='text' name='id' value='" . uniqid() . "' style='visibility:hidden; height:0px' readonly>";
			// Resten får användaren fylla i.
			$name = "";
			$surName = "";
			$telephone = "";
			$email = "";
			$image = "image";
		}
	} catch(Exception $e) {
	}

	// Ställer iordning ett formulär som antingen innehåller tidigare värden eller ingenting
	// alls beroende på vad användaren har valt att göra (skapa nytt visitkort eller uppdatera ett befintligt).
	echo "<form method='post' enctype='multipart/form-data'>"
				. $id . 
				"<div class='form-group'>
					<label for='inputName'>Förnamn</label>
					<input type='text' id='inputName' name='name' value='" . $name . "' autocomplete='off' class='form-control' required>
				</div>
				<div class='form-group'>
					<label for='inputSurName'>Efternamn</label>
					<input type='text' id='inputSurName' name='surName' value='" . $surName . "' autocomplete='off' class='form-control' required>
				</div>
				<div class='form-group'>
					<label for='inputTelephone'>Telefonnummer</label>
					<input type='tel' id='inputTelephone' name='telephone' value='" . $telephone . "' autocomplete='off' class='form-control' required>
				</div>
				<div class='form-group'>
					<label for='inputEmail'>E-postadress</label>
					<input type='email' id='inputEmail' name='email' value='" . $email . "' autocomplete='off' class='form-control' required>
				</div>
				<div class='form-group'>
					<label for='inputImage'>Bild</label>
					<input type='file' accept='image/x-png,image/jpeg' id='inputImage' name='" . $image . "' id='fileToUpload' class='form-control-file' required>
				</div>
				<input type='submit' value='Spara visitkort' class='btn btn-primary'>
			</form>";
?>