CREATE TABLE table_name (
	"id" text PRIMARY KEY NOT NULL,
    "name" text,
    "surName" text,
    telephone text,
	email text,
	image bytea
); 